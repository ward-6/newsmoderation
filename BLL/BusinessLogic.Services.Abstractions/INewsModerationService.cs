using BusinessLogic.Contracts;

namespace BusinessLogic.Services.Abstractions;


public interface INewsItemModerationService
{
    int CanModerateCount();

    int ModeratedCount();

    bool PushToModeration(NewsItemModerationDto dto);

    void PopModerated();

    NewsItemModerationDto? NextModerated(); 

    void Moderate(CancellationToken stoppingToken, int maxModerationTime = int.MaxValue);
    
}