

using System.Collections.ObjectModel;

namespace BusinessLogic.Services.Abstractions;

public interface ISender<T>
{
    int Send(IEnumerable<T> items);

    bool WaitAck(int timeoutMs);

}

public interface IReceiver<T>
{
    ReadOnlyCollection<T> Receive(int maxCount);

    void Ack();
}



public interface ITransferService<T>
{
    ISender<T> Sender { get; }

    IReceiver<T> Receiver { get; }
}