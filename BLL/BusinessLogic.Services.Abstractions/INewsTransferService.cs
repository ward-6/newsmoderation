using System.Collections.ObjectModel;
using BusinessLogic.Contracts;

namespace BusinessLogic.Services.Abstractions;


public interface INewsTransferService : ITransferService<ModerationMessageDto>
{
    bool Initialize(int initTimeoutMs, CancellationToken token);

    bool IsInitialized();

}