

using System.Text;

namespace BusinessLogic.Contracts;

public class NewsItemModerationDto
{
    public int Id { get; set; }
    public string Title { get; set; } = string.Empty;
    public string Text { get; set; } = string.Empty;
    public DateTime ModificationTime { get; set; }
    public List<string> Tags { get; set; } = new List<string>();
    public bool Approved { get; set; }
    public string ModeratorComment { get; set; } = string.Empty;

    public byte[] GetBytes()
    {
        List<byte> bytes = new List<byte>();
        bytes.AddRange(BitConverter.GetBytes(Id));
        bytes.AddRange(StringToBytes(Title));
        bytes.AddRange(StringToBytes(Text));
        bytes.AddRange(BitConverter.GetBytes(Tags.Count));
        foreach (string tag in Tags)
        {
            bytes.AddRange(StringToBytes(tag));
        }

        bytes.AddRange(BitConverter.GetBytes(Approved));
        bytes.AddRange(StringToBytes(ModeratorComment));
        bytes.AddRange(BitConverter.GetBytes(ModificationTime.Ticks));
        return bytes.ToArray();
    }

    public static bool TryParse(byte[] bytes, out NewsItemModerationDto result)
    {
        var newsItem = new NewsItemModerationDto();
        try
        {
            int offset = 0;
            newsItem.Id = BitConverter.ToInt32(bytes);
            offset += 4;
            string tmp = string.Empty;
            offset += ParseString(bytes,offset,out tmp);
            newsItem.Title = tmp;
            tmp = string.Empty;
            offset += ParseString(bytes,offset,out tmp);
            newsItem.Text = tmp;
            int tagsCount = BitConverter.ToInt32(bytes, offset);
            offset += 4;
            for (int i = 0; i < tagsCount; i++)
            {
                tmp = string.Empty;
                offset += ParseString(bytes,offset,out tmp);
                newsItem.Tags.Add(tmp);
            }
            newsItem.Approved = BitConverter.ToBoolean(bytes, offset);
            offset += 1;
            tmp = string.Empty;
            offset+=ParseString(bytes,offset,out tmp);
            newsItem.ModeratorComment = tmp;
            long ticks = BitConverter.ToInt64(bytes,offset);
            newsItem.ModificationTime = new DateTime(ticks, DateTimeKind.Utc);
        }
        catch (Exception)
        {
            result = newsItem;
            return false;
        }

        result = newsItem;
        return true;
    }

    public override bool Equals(object? obj)
    {
        //Check for null and compare run-time types.
        if ((obj == null) || !this.GetType().Equals(obj.GetType()))
        {
            return false;
        }
        else
        {
            var other = (NewsItemModerationDto)obj;

            if (Id != other.Id) return false;
            if (Title != other.Title) return false;
            if (Text != other.Text) return false;
            if (Approved != other.Approved) return false;
            if (ModeratorComment != other.ModeratorComment) return false;
            if (Tags.Count != other.Tags.Count) return false;
            if (ModificationTime!= other.ModificationTime) return false;

            for (int i = 0; i < Tags.Count; i++)
            {
                if (Tags[i] != other.Tags[i])
                {
                    return false;
                }
            }

            return true;

        }
    }

    private byte[] StringToBytes(string str)
    {
        List<byte> result = new List<byte>();
        byte[] stringBytes = Encoding.UTF8.GetBytes(str);
        result.AddRange(BitConverter.GetBytes(stringBytes.Length));
        result.AddRange(stringBytes);
        return result.ToArray();
    }

    private static int ParseString(byte[] arr, int offset, out string result)
    {
        int stringLen = BitConverter.ToInt32(arr, offset);
        result = Encoding.UTF8.GetString(arr, offset+4, stringLen);
        return stringLen+4;
    }
}