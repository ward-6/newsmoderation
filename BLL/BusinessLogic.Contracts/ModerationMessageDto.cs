

namespace BusinessLogic.Contracts;

public enum ModerationMessageId
{
    Unknown = 0,
    NewsItem = 0x2251
};


public class ModerationMessageDto
{
    private byte[] _body = new byte[] { };

    protected ModerationMessageDto() { }

    protected ModerationMessageDto(ModerationMessageId id, byte[] body)
    {
        Id = id;
        _body = body;
    }

    public ModerationMessageDto(NewsItemModerationDto newsItem)
    {
        Id = ModerationMessageId.NewsItem;
        _body = newsItem.GetBytes();
    }
    public ModerationMessageId Id { get; } = ModerationMessageId.Unknown;
    public ReadOnlyMemory<byte> Body { get { return new ReadOnlyMemory<byte>(_body); } }
    public static ModerationMessageDto? TryParse(byte[] data)
    {
        int tmpId = BitConverter.ToInt32(data);
        ModerationMessageId parsedId = ModerationMessageId.Unknown;
        foreach (var id in Enum.GetValues<ModerationMessageId>())
        {
            if (((int)id == tmpId) && (id != ModerationMessageId.Unknown))
            {
                parsedId = id;
                break;
            }
        }

        if (parsedId != ModerationMessageId.Unknown)
        {
            byte[] body = new byte[data.Length - 4];
            Array.Copy(data, 4, body, 0, body.Length);
            return new ModerationMessageDto(parsedId, body);
        }
        return null;
    }

    public byte[] GetBytes()
    {
        List<byte> bytes = new List<byte>();
        if (Id != ModerationMessageId.Unknown)
        {
            bytes.AddRange(BitConverter.GetBytes((int)Id));
            bytes.AddRange(_body);
        }
        return bytes.ToArray();
    }
}