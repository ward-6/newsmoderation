using BusinessLogic.Contracts;
using BusinessLogic.Services.Abstractions;
using Microsoft.Extensions.Logging;

namespace BusinessLogic.Services.Implementations;

public class NewsItemModerationServiceOptions
{
    public int ModerationProbability { get; set; }

    public int OnModerationLimit { get; set; }

    public int ModerationEmulatedDurationMs { get; set; }
}

public class NewsItemModerationService : INewsItemModerationService
{
    private Queue<NewsItemModerationDto> _beforeModerationQueue = new Queue<NewsItemModerationDto>();

    private Queue<NewsItemModerationDto> _afterModerationQueue = new Queue<NewsItemModerationDto>();

    private readonly NewsItemModerationServiceOptions _options;

    private readonly ILogger<NewsItemModerationService> _logger;

    public NewsItemModerationService(NewsItemModerationServiceOptions options, ILogger<NewsItemModerationService> logger)
    {
        _options = options;
        _logger = logger;
    }

    public int CanModerateCount()
    {
        return _options.OnModerationLimit - _beforeModerationQueue.Count;
    }

    public int ModeratedCount()
    {
        return _afterModerationQueue.Count;
    }

    public NewsItemModerationDto? NextModerated()
    {
        NewsItemModerationDto? dtoresult = null;
        NewsItemModerationDto? dtopeek;
        if (_afterModerationQueue.TryPeek(out dtopeek))
        {
            dtoresult = dtopeek;
        }
        return dtoresult;
    }

    public void PopModerated()
    {
        NewsItemModerationDto? dto;
        _afterModerationQueue.TryDequeue(out dto);
        _logger.LogInformation("popped from moderator");
    }

    public bool PushToModeration(NewsItemModerationDto dto)
    {
        if (_beforeModerationQueue.Count < _options.OnModerationLimit)
        {
            _beforeModerationQueue.Enqueue(dto);
            _logger.LogInformation("pushed to moderator");
            return true;
        }
        return false;
    }

    public void Moderate(CancellationToken stoppingToken, int maxModerationTime = int.MaxValue)
    {
        Random r = new Random();

        if (_afterModerationQueue.Count < _options.OnModerationLimit)
        {
            NewsItemModerationDto? dto;
            if (_beforeModerationQueue.TryDequeue(out dto))
            {
                dto.Approved = ((_options.ModerationProbability + r.Next(0, 100)) >= 100);

                if (!dto.Approved)
                {
                    dto.ModeratorComment = "rejected by random algorithm at " + DateTime.Now;
                }
                stoppingToken.WaitHandle.WaitOne(_options.ModerationEmulatedDurationMs);
                _logger.LogInformation("moderated");
                _afterModerationQueue.Enqueue(dto);
            }
        }
    }
}