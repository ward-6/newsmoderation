using System.Collections.ObjectModel;
using System.Diagnostics;
using BusinessLogic.Contracts;
using BusinessLogic.Services.Abstractions;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace BusinessLogic.Services.Implementations;

public class RabbitMqTransferOptions
{
    public string ConnectionString { get; set; } = string.Empty;

    public string ModerationQueueRoute { get; set; } = string.Empty;

    public string PublicationQueueRoute { get; set; } = string.Empty;
}

public class RabbitMqTransferService : INewsTransferService, IDisposable
{
    private IConnection? _connection = null;

    private IModel? _receiveChannel = null;

    private IModel? _sendChannel = null;

    private RabbitMQ.Client.IBasicProperties? _sendProperties = null;

    private readonly RabbitMqTransferOptions _options;

    private readonly ILogger<RabbitMqTransferService> _logger;

    private bool _initialized = false;

    private SenderImpl _sender;

    private ReceiverImpl _receiver;

    public RabbitMqTransferService(RabbitMqTransferOptions opts, ILogger<RabbitMqTransferService> logger)
    {
        _options = opts;
        _logger = logger;
        _sender = new SenderImpl(this);
        _receiver = new ReceiverImpl(this);
    }

    public ISender<ModerationMessageDto> Sender => _sender;

    public IReceiver<ModerationMessageDto> Receiver => _receiver;

    public bool Initialize(int initTimeoutMs, CancellationToken token)
    {
        if (_initialized) return true;

        Stopwatch timer = new Stopwatch();
        timer.Start();

        RabbitMQ.Client.ConnectionFactory factory = new RabbitMQ.Client.ConnectionFactory
        {
            Uri = new System.Uri(_options.ConnectionString)
        };

        factory.AutomaticRecoveryEnabled = true;

        while (_connection == null)
        {
            if (token.IsCancellationRequested) { return false; }

            if (timer.ElapsedMilliseconds>=initTimeoutMs) { return false; }

            try
            {
                _connection = factory.CreateConnection();
            }
            catch (Exception)
            {
                _logger.LogWarning("fail to connect rabbitmq server");
                token.WaitHandle.WaitOne(5000);
            }
        }

        while (_sendChannel == null)
        {
            if (token.IsCancellationRequested) { return false;}

            if (timer.ElapsedMilliseconds>=initTimeoutMs) { return false; }
            
            try
            {
                _sendChannel = _connection.CreateModel();
                _sendChannel.ConfirmSelect();
            }
            catch (Exception)
            {
                token.WaitHandle.WaitOne(5000);
            }
        }

        while (_receiveChannel == null)
        {
            if (token.IsCancellationRequested) { return false;}

            if (timer.ElapsedMilliseconds>=initTimeoutMs) { return false; }

            try
            {
                _receiveChannel = _connection.CreateModel();
            }
            catch (Exception)
            {
                token.WaitHandle.WaitOne(5000);
            }
        }

        bool sendQueueDeclared = false;
        while (!sendQueueDeclared)
        {
            if (token.IsCancellationRequested) { return false;}

            if (timer.ElapsedMilliseconds>=initTimeoutMs) { return false; }

            try
            {
                _sendChannel.QueueDeclare(queue: _options.PublicationQueueRoute,
                     durable: true,
                     exclusive: false,
                     autoDelete: false,
                     arguments: null);

                _sendProperties = _sendChannel.CreateBasicProperties();
                _sendProperties.DeliveryMode = 2; //persistent

                sendQueueDeclared = true;
            }
            catch
            {
                token.WaitHandle.WaitOne(5000);
            }
        }

        bool receiveQueueDeclared = false;
        while (!receiveQueueDeclared)
        {
            if (token.IsCancellationRequested) { return false;}

            if (timer.ElapsedMilliseconds>=initTimeoutMs) { return false; }

            try
            {
                _receiveChannel.QueueDeclare(queue: _options.ModerationQueueRoute,
                     durable: true,
                     exclusive: false,
                     autoDelete: false,
                     arguments: null);

                receiveQueueDeclared = true;
            }
            catch
            {
                token.WaitHandle.WaitOne(5000);
            }
        }

        if (token.IsCancellationRequested)
        {
            return false;
        }

        _initialized = true;
        return _initialized;
    }

    public bool IsInitialized()
    {
        return _initialized;
    }

    public void Dispose()
    {
        _connection?.Close();
        _connection?.Dispose();
    }

    private class SenderImpl : ISender<ModerationMessageDto>
    {
        private readonly RabbitMqTransferService _service;
        public SenderImpl(RabbitMqTransferService owner)
        {
            _service = owner;
        }
        public int Send(IEnumerable<ModerationMessageDto> items)
        {
            if (_service._sendChannel == null) return 0;
            int count = 0;
            try
            {
                foreach (var dto in items)
                {
                    byte[] msg = dto.GetBytes();
                    _service._sendChannel.BasicPublish(exchange: "",
                                 routingKey: _service._options.PublicationQueueRoute,
                                 basicProperties: _service._sendProperties,
                                 body: msg);

                    count++;
                    _service._logger.LogInformation("Отправлено " + msg.Length + " байт в очередь " + _service._options.PublicationQueueRoute);
                }
            }
            catch (Exception e)
            {
                _service._logger.LogWarning("Ошибка отправки в очередь " + _service._options.PublicationQueueRoute);
                _service._logger.LogWarning(e.Message);
            }

            return count;
        }

        public bool WaitAck(int timeoutMs)
        {
            bool result = false;

            if (_service._sendChannel != null)
            {
                try
                {
                    _service._sendChannel.WaitForConfirmsOrDie(TimeSpan.FromMilliseconds(timeoutMs));
                    result = true;
                }
                catch (Exception)
                {
                    _service._logger.LogWarning("От брокера не получено подтверждение о доставке в очередь " + _service._options.PublicationQueueRoute);
                }
            }
            return result;
        }
    }

    private class ReceiverImpl : IReceiver<ModerationMessageDto>
    {
        private class ReceiveData
        {
            public ulong DeliveryTag { get; set; }
            public ModerationMessageDto Data { get; set; } 

            public ReceiveData(ulong tag, ModerationMessageDto dto)
            {
                DeliveryTag = tag;
                Data = dto;
            }
        };
        private readonly RabbitMqTransferService _service;
        private Queue<ReceiveData> _receiveQueue = new Queue<ReceiveData>();
        private EventingBasicConsumer? _consumer = null;
        private int _consumerPrefetchCount = 0;
        private int _receivedCount = 0;
        private bool _consumerAttached = false;
        public ReceiverImpl(RabbitMqTransferService owner)
        {
            _service = owner;
        }

        public ReadOnlyCollection<ModerationMessageDto> Receive(int maxCount)
        {
            try
            {
                if (_service._receiveChannel != null)
                {
                    if (maxCount != _consumerPrefetchCount)
                    {
                        _service._receiveChannel.BasicQos(0, (ushort)maxCount, false);
                        _consumerPrefetchCount = maxCount;
                    }

                    if (_consumer == null)
                    {
                        _consumer = new RabbitMQ.Client.Events.EventingBasicConsumer(_service._receiveChannel);
                        _consumer.Received += (model, ea) =>
                        {
                            var body = ea.Body;

                            ModerationMessageDto? message = ModerationMessageDto.TryParse(body.ToArray());

                            if (message!=null)
                            {
                                _receiveQueue.Enqueue(new ReceiveData(ea.DeliveryTag, message));
                            }
                            else
                            {
                                _service._receiveChannel.BasicReject(ea.DeliveryTag, true);
                            }

                            _service._logger.LogInformation("Получено " + body.Length + " байт из очереди " + _service._options.ModerationQueueRoute);

                        };

                    }


                    if (!_consumerAttached)
                    {
                        _service._receiveChannel.BasicConsume(queue: _service._options.ModerationQueueRoute,
                                             autoAck: false,
                                             consumer: _consumer);
                        _consumerAttached = true;
                    }
                    
                }


            }
            catch
            {

            }

            _receivedCount = _receiveQueue.Count;
            return new ReadOnlyCollection<ModerationMessageDto>(_receiveQueue.Take(_receivedCount).Select(it => it.Data).ToList());
        }

        public void Ack()
        {
            try
            {
                if ((_service._receiveChannel != null)&&(_receivedCount>0))
                {
                    _service._receiveChannel.BasicAck(_receiveQueue.AsEnumerable().ElementAt(_receivedCount-1).DeliveryTag, true);
                    for(int i=0;i<_receivedCount;i++) _receiveQueue.Dequeue();
                }
            }
            catch
            {

            }
        }
    }
}