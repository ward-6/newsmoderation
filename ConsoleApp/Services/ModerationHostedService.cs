using Microsoft.Extensions.Hosting;
using BusinessLogic.Contracts;
using BusinessLogic.Services.Abstractions;
using Microsoft.Extensions.Logging;

namespace ConsoleApp.Services;

public class ModerationHostedServiceOptions
{
    public int ModerationPeriodMs { get; set; }

}

public class ModerationHostedService : BackgroundService
{
    private readonly INewsItemModerationService _moderationService;

    private readonly INewsTransferService _connService;

    private readonly ModerationHostedServiceOptions _options;

    private readonly ILogger<ModerationHostedService> _logger;
    public ModerationHostedService(INewsItemModerationService modService, INewsTransferService connService, ModerationHostedServiceOptions opts, ILogger<ModerationHostedService> logger)
    {
        _moderationService = modService;
        _connService = connService;
        _options = opts;
        _logger = logger;
    }
    protected override Task ExecuteAsync(CancellationToken stoppingToken)
    {
        return Task.Factory.StartNew(() => 
        {
            while(!_connService.IsInitialized())
            {
                _connService.Initialize(30000,stoppingToken);
                if (stoppingToken.IsCancellationRequested)
                {
                    break;
                }
            }

            if (stoppingToken.IsCancellationRequested) return;

            Thread moderationThread = new Thread(ModerationEntryPoint);
            moderationThread.IsBackground = true;
            moderationThread.Start(stoppingToken);
            moderationThread.Join();
        }, TaskCreationOptions.LongRunning);
    }

    private void ModerationEntryPoint(object? token)
    {
        CancellationToken stoppingToken = (CancellationToken)(token ?? throw new NullReferenceException("wrong cancellationtoken"));
        List<ModerationMessageDto> messageList = new List<ModerationMessageDto>();
        int lastCount = 0;
        bool canReceive = true;
        int receiveLimit = _moderationService.CanModerateCount();
        while (!stoppingToken.IsCancellationRequested)
        {
            if (canReceive)
            {
                var messages = _connService.Receiver.Receive(receiveLimit);


                if (((lastCount == messages.Count) && (messages.Count > 0)) ||
                    (messages.Count>=receiveLimit))
                {
                    foreach(var msg in messages)
                    {
                        messageList.Add(msg);
                    }
                    _connService.Receiver.Ack();
                    canReceive = false;
                    lastCount = 0;
                }

                lastCount = messages.Count;
            }


            Queue<NewsItemModerationDto> unckeckedNews = new Queue<NewsItemModerationDto>();
            foreach(var msg in messageList)
            {
                
                if (msg.Id == ModerationMessageId.NewsItem)
                {
                    NewsItemModerationDto dto;
                    if (NewsItemModerationDto.TryParse(msg.Body.ToArray(),out dto))
                    {
                        unckeckedNews.Enqueue(dto);
                    }   
                }
            }

            messageList.Clear();

            while (unckeckedNews.Count > 0)
            {
                var item = unckeckedNews.Peek();
                if (!_moderationService.PushToModeration(item))
                {
                    break;
                }
                unckeckedNews.Dequeue();
            }

            if (unckeckedNews.Count==0)
            {
                canReceive = true;
            }

            _moderationService.Moderate(stoppingToken);



            while (_moderationService.ModeratedCount() > 0)
            {
                NewsItemModerationDto? moderated = _moderationService.NextModerated();
                if (moderated != null)
                {
                    if (_connService.Sender.Send(new List<ModerationMessageDto>(){new ModerationMessageDto(moderated)})>0)
                    {
                        if (_connService.Sender.WaitAck(5000))
                        {
                            _moderationService.PopModerated();
                        }
                        
                    }
                    
                }
            }

            stoppingToken.WaitHandle.WaitOne(_options.ModerationPeriodMs);
        }

    }

}