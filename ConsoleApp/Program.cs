﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using BusinessLogic.Services.Abstractions;
using BusinessLogic.Services.Implementations;

var host = Host.CreateDefaultBuilder().ConfigureAppConfiguration(
    cfg =>
    {
        cfg.SetBasePath(Directory.GetCurrentDirectory());
        cfg.AddJsonFile("appsettings.json");
    }
).ConfigureServices((context,services) => 
{

    services
    .AddSingleton<RabbitMqTransferOptions>(
        context.Configuration.GetSection(nameof(RabbitMqTransferOptions)).Get<RabbitMqTransferOptions>() ??
        throw new InvalidOperationException("wrong "+nameof(RabbitMqTransferOptions))
    )
    .AddSingleton<NewsItemModerationServiceOptions>(
        context.Configuration.GetSection(nameof(NewsItemModerationServiceOptions)).Get<NewsItemModerationServiceOptions>() ??
        throw new InvalidOperationException("wrong "+nameof(NewsItemModerationServiceOptions))
    )
    .AddSingleton<ConsoleApp.Services.ModerationHostedServiceOptions>(
        context.Configuration.GetSection(nameof(ConsoleApp.Services.ModerationHostedServiceOptions)).Get<ConsoleApp.Services.ModerationHostedServiceOptions>() ??
        throw new InvalidOperationException("wrong "+ nameof(ConsoleApp.Services.ModerationHostedServiceOptions))
    )
    .AddSingleton<INewsItemModerationService, NewsItemModerationService>()
    .AddSingleton<INewsTransferService, RabbitMqTransferService>()
    .AddHostedService<ConsoleApp.Services.ModerationHostedService>();

    
}).Build();

host.Run();